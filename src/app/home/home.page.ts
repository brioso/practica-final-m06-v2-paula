import { Component } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { MenuController } from 'node_modules/@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private router: Router, private menu: MenuController) {
    this.getJson();
  }

  motos;

  async getJson(){
    const respuesta = await fetch("http://paula-brioso-7e3.alwaysdata.net/miapi/getMotos");
    this.motos = await respuesta.json();
    this.menu.close();
  }

  async getMarcaX(marca: string){
    var link: string = "http://paula-brioso-7e3.alwaysdata.net/miapi/getMarceX?marca=" + marca;
    const respuesta = await fetch(link);
    this.motos = await respuesta.json();
    this.menu.close();
  }

  detalle(moto) {
    let navigationExtras: NavigationExtras = {
      state: {
        parametros: moto,
      }
    };
    this.router.navigate(['moto-page'], navigationExtras);
  }

  addMoto() {
    this.router.navigate(['add-moto-page']);
  }

  ionViewDidEnter(){
    this.getJson();
  }
}
