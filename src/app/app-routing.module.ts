import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'moto-page',
    loadChildren: () => import('./moto-page/moto-page.module').then( m => m.MotoPagePageModule)
  },
  {
    path: 'add-moto-page',
    loadChildren: () => import('./add-moto-page/add-moto-page.module').then( m => m.AddMotoPagePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
