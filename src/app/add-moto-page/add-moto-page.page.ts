import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-add-moto-page',
  templateUrl: './add-moto-page.page.html',
  styleUrls: ['./add-moto-page.page.scss'],
})
export class AddMotoPagePage implements OnInit {
  marcaForm;
  modeloForm;
  anoForm;
  precioForm;

  constructor(private router: Router) { }

  addMoto() {
    var motillo = {
      'marca': this.marcaForm,
      'modelo': this.modeloForm,
      'year': this.anoForm,
      'precio': this.precioForm,
      'foto': 'https://image.flaticon.com/icons/png/512/62/62620.png'
    }

    //var motoString: string = "{\"marca\":\"1111111111111111111\",\"modelo\":\"11111111111111crambler Icon 2019\",\"year\":\"2019\",\"foto\":\"http://motos-b60.kxcdn.com/sites/default/files/ducati-scrambler-icon-2019.jpg\",\"precio\":\"9.190€\"}"

    fetch("http://paula-brioso-7e3.alwaysdata.net/miapi/moto", {
    "method": "POST",
    "headers": {
      "Content-Type": "application/json"
    },
    "body": JSON.stringify(motillo)
  })
  .then(response => {
    console.log(response);
  })
  .catch(err => {
    console.error(err);
  });
    
  this.router.navigate(['home']);

  }

  

  ngOnInit() {
  }

}
