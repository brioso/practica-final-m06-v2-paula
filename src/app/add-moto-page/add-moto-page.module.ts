import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddMotoPagePageRoutingModule } from './add-moto-page-routing.module';

import { AddMotoPagePage } from './add-moto-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddMotoPagePageRoutingModule
  ],
  declarations: [AddMotoPagePage]
})
export class AddMotoPagePageModule {}
