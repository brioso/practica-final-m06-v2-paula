import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddMotoPagePage } from './add-moto-page.page';

const routes: Routes = [
  {
    path: '',
    component: AddMotoPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddMotoPagePageRoutingModule {}
