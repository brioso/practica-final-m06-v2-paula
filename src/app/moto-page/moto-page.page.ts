import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';


@Component({
  selector: 'app-moto-page',
  templateUrl: './moto-page.page.html',
  styleUrls: ['./moto-page.page.scss'],
})
export class MotoPagePage implements OnInit {

  moto;

  constructor(private route: ActivatedRoute, private router: Router, public alertController: AlertController) {
    
    this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.moto = this.router.getCurrentNavigation().extras.state.parametros;
      }
    });
  }

  async presentAlertMultipleButtons(moto) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Compte',
      subHeader: 'Eliminant',
      message: 'Esborrarà la moto.',
      buttons: [
        {text: 'Cancel',
        role: 'cancel',
        handler: (blah) => {
          console.log("canceled");
        }},
        {text: 'Delete',
        handler: () => {
          this.deleteMoto(moto);
        }}]
    });

    await alert.present();

    let result = await alert .onDidDismiss();
    console.log(result);
  }

  deleteMoto(moto) {
    console.log('gfgfgfsgfsgfd' + moto.id);

    const url = "http://paula-brioso-7e3.alwaysdata.net/miapi/moto/" + moto.id;

    
    fetch(url, {
      "method": "DELETE",
      "headers": {}
    })
    .then(response => {
      console.log(response);
    })
    .catch(err => {
      console.error(err);
    });

    this.router.navigate(['home']);
  }

  

  ngOnInit() {
  }

}
