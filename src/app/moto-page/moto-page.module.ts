import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MotoPagePageRoutingModule } from './moto-page-routing.module';

import { MotoPagePage } from './moto-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MotoPagePageRoutingModule
  ],
  declarations: [MotoPagePage]
})
export class MotoPagePageModule {}
