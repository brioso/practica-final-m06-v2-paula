import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MotoPagePage } from './moto-page.page';

const routes: Routes = [
  {
    path: '',
    component: MotoPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MotoPagePageRoutingModule {}
